'use strict';
var request = require('request');
var loopback = require('loopback');
module.exports = function(Cultuser) {
    delete Cultuser.validations.email;
    var cultUserCollection = loopback.getModel('cultUser');

    Cultuser.forgotPassword = function(cultUsername,callback){
        var otp = getRndInteger(1000, 9999);
        cultUserCollection.update({username:cultUsername},{cultOtp:otp.toString()},function(err,result){
            if(err){
                console.log("OTP update error for:"+cultUsername);
            }
            else{
                console.log("OTP update successful for :"+cultUsername);
                request.get({
                    headers: {'content-type' : 'application/x-www-form-urlencoded'},
                    url:     'https://api.msg91.com/api/sendhttp.php?mobiles='+cultUsername+'&authkey=193817AqQFNTtmJQ5a607ed2&route=4&sender=CULTIN&message=Hello! Your one time password is -'+otp+'&country=91&otp='+otp,
                    //json:true,
                    //body: {"otp":5588,"sender":"cultmg","message":"Hello!! Your otp is:5588","mobile":"8793889599","authkey":"193817AqQFNTtmJQ5a607ed2"}
                }, function(error, response, body){
                    if(error){
                        var result = {"success":false};
                        callback(null,result);
                    }
                    else{
                        var result = {"success":true};
                        callback(null,result);
                    }
                });

            }
        });

    }
    Cultuser.resetPassword = function(cultUsername,cultOtp,cultPassword,callback){
        Cultuser.find({where:{username:cultUsername}},function(err, user) {
            
            if(cultOtp == user[0].cultOtp){
                cultUserCollection.update({username:cultUsername},{password:Cultuser.hashPassword(cultPassword)},function(err,res){
                    if(err){
                        var result = {"success":false};
                        callback(null,result);
                    }
                    else{
                        var result = {"success":true};
                        callback(null,result);  
                    }
                });
                
            }
            else{

                var result = {"success":false};
                callback(null,result);
            }
        })
    }
    Cultuser.verifyOtp = function(cultUsername,cultOtp,callback){
        //console.log(cultUsername);
        Cultuser.find({where:{username:cultUsername}},function(err, user) {
            //console.log(user);
            //console.log(cultOtp);
	    //console.log(user[0].cultOtp);
            if(cultOtp == user[0].cultOtp){
                cultUserCollection.update({username:cultUsername},{cultVerification:true},function(err,res){
                    if(err){
                        var result = {"success":false};
                        callback(null,result);
                    }
                    else{
                        var result = {"success":true};
                        callback(null,result);
                    }
                });
                
            }
            else{

                var result = {"success":false};
                callback(null,result);
            }
        })

    }
    Cultuser.afterRemote('create',function(ctx,res,next){
        
        console.log(ctx.result);
        //

        var otp = getRndInteger(1000, 9999);
        cultUserCollection.update({username:ctx.result.username},{cultOtp:otp.toString()},function(err,result){
            if(err){
                console.log("OTP update error for:"+ctx.result.username);
            }
            else{
                console.log("OTP update successful for :"+ctx.result.username);
                request.get({
                    headers: {'content-type' : 'application/x-www-form-urlencoded'},
                    url:     'https://api.msg91.com/api/sendhttp.php?mobiles='+ctx.result.cultContact+'&authkey=193817AqQFNTtmJQ5a607ed2&route=4&sender=CULTIN&message=Hello! Your one time password is -'+otp+'&country=91&otp='+otp,
                    //json:true,
                    //body: {"otp":5588,"sender":"cultmg","message":"Hello!! Your otp is:5588","mobile":"8793889599","authkey":"193817AqQFNTtmJQ5a607ed2"}
                }, function(error, response, body){
                    console.log(body);
                });

            }
        });
        //otp=5588&sender=cultmg&message=hello&mobile=8793889599&authkey=193817AqQFNTtmJQ5a607ed2',
        next();
    });

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    }

    Cultuser.remoteMethod('forgotPassword', {
        accepts: [
            {
                "arg": "cultUsername",
                "type": "string",
                "required": true,
                "description": "",
                
            }
        ],
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/forgotPassword",
            "verb": "post"
          }
        ]
    });

    Cultuser.remoteMethod('resetPassword', {
        accepts: [
            {
                "arg": "cultUsername",
                "type": "string",
                "required": true,
                "description": "",
                
            },
            {
                "arg": "cultOtp",
                "type": "string",
                "required": true,
                "description": "",
                
            },
            {
                "arg": "cultPassword",
                "type": "string",
                "required": true,
                "description": "",
                
            }
        ],
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/resetPassword",
            "verb": "post"
          }
        ]
    });
    Cultuser.remoteMethod('verifyOtp', {
        accepts: [
            {
                "arg": "cultUsername",
                "type": "string",
                "required": true,
                "description": "",
                
            },
            {
                "arg": "cultOtp",
                "type": "string",
                "required": true,
                "description": "",
                
            }
        ],
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/verifyOtp",
            "verb": "post"
          }
        ]
    });

    

};
