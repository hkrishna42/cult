'use strict';
var loopback = require('loopback');
module.exports = function(Cultbuy) {
    var cultUserCollection = loopback.getModel('cultUser');
    
    Cultbuy.getUserBottles = function(userNumber,callback){
        cultUserCollection.find({where:{username:userNumber}},function(err, user) {
            if(err){
                result = {"message": false}
                callback(null,result)
            }
            else{
                console.log((user[0].id).toString());
                var userId = (user[0].id).toString();
                Cultbuy.find({include:"product"},{where:{cultUserId:userId}},function(err,buy){
                    if(err){
                        result = {"message": false}                   
                    }
                    else{
                        var result = {
                            message:true,
                            userDetails:user,
                            buyDetails:buy
                        };
                        callback(null,result);
                    }
                    
                })
            }
            
        })
        //console.log(userNumber);
       // callback(null,userNumber);
    }
    Cultbuy.remoteMethod('getUserBottles', {
        accepts: [
            {
                "arg": "userNumber",
                "type": "string",
                "required": true,
                "description": "",
                'http':{source:'query'}
                
            }
        ],
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/getUserBottles",
            "verb": "get"
          }
        ]
    });

};
