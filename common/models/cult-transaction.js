'use strict';
var request = require('request');
var loopback = require('loopback');
module.exports = function(Culttransaction) {
    var cultTransactionCollection = loopback.getModel('cultTransaction');
    var cultBuyCollection = loopback.getModel('cultBuy');
    var cultUserCollection = loopback.getModel('cultUser');
    Culttransaction.afterRemote('create',function(ctx,res,next){
        console.log(ctx.result);
        var transactionId = ctx.result.id;
        var otp = getRndInteger(1000, 9999);
        cultUserCollection.findById(ctx.result.cultUserId,function(userError,userResult){
            if(userError){
                var result = {"message":false};
                //callback(null,result);
            }
            else{
                var result = userResult;
                console.log(result);
                //callback(null,result);
                cultTransactionCollection.update({id:transactionId},{cultTransactionOtp:otp.toString()},function(err,result){
                    if(err){
                        console.log("OTP update error for:"+transactionId);
                    }
                    else{
                        console.log("OTP update successful for :"+transactionId);
        
                        request.get({
                            headers: {'content-type' : 'application/x-www-form-urlencoded'},
                            url:     'https://api.msg91.com/api/sendhttp.php?mobiles='+userResult.username+'&authkey=193817AqQFNTtmJQ5a607ed2&route=4&sender=CULTIN&message=Hello! Your one time password for the transaction is -'+otp+'&country=91&otp='+otp,
                            //json:true,
                            //body: {"otp":5588,"sender":"cultmg","message":"Hello!! Your otp is:5588","mobile":"8793889599","authkey":"193817AqQFNTtmJQ5a607ed2"}
                        }, function(error, response, body){
                            if(error){
                                var result = {"success":false};
                                console.log(result);
                                //callback(null,result);
                            }
                            else{
                                var result = {"success":true};
                                console.log(result);
                                //callback(null,result);
                            }
                        });
        
                    }
                });
            }
        });
        /**/
        next();

    });
    Culttransaction.afterRemote('transactionVerification',function(ctx,res,next){

          var cultBuyId = ctx.result.transaction.cultBuyId;
	  console.log(ctx.result);
          cultBuyCollection.findById(cultBuyId,function(buyError,buyResult){
            if(buyError){
		console.log(buyError);
                console.log("Error in finding the buy data.");
            }
            else{
		console.log(buyResult);
                var buyQuantity = parseInt(buyResult.cultBuyRemaining);
                var transactionQuantity = parseInt(ctx.result.transaction.cultTransactionQuantity);
                var cultBuyRemaining = buyQuantity - transactionQuantity;
                cultBuyCollection.update({id:cultBuyId},{cultBuyRemaining:cultBuyRemaining.toString()},function(updateError,updateResult){
                    if(updateError){
                        console.log("Error: in updating Buy ID:"+cultBuyId);
                    }
                    else{
                        console.log("Buy Remaining is successful for:"+cultBuyId);
                    }
                });
            }
          })
                            /*
                            var cultBuyRemaining = cult
                            */
                           
        next();

    });
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    }

    Culttransaction.dashboard = function(cultUserId,callback){
        Culttransaction.find({where:{cultUserId:cultUserId},include:"product"},{limit: 5},function(transactionError,transactionResult){
            if(transactionError){
                callback(null,{"message":false});
            }
            else{
                cultBuyCollection.find({where:{cultUserId:cultUserId},include:"product"},function(buyError,buyResult){
                    if(buyError){
                        callback(null,{"message":false});
                    }
                    else{
                        var result = {bottles:buyResult,transactions:transactionResult};
                        callback(null,result);
                    }
                });
                
            }
        });
    }

    Culttransaction.transactionVerification = function(cultTransactionId,cultTransactionOtp,callback){
        Culttransaction.findById(cultTransactionId,function(transactionError,transactionResult){
            if(transactionError){
                callback(null,{message:false});
            }
            else{
                //callback(null,transactionResult);
                if(cultTransactionOtp == (transactionResult.cultTransactionOtp).toString()){
                    Culttransaction.update({id:cultTransactionId},{cultTransactionVerified:true},function(updateError,updateResult){
                        if(updateError){
                            callback(null,{message:false});
                        }
                        else{
                            callback(null,{message:true,transaction:transactionResult});
                        }
                        
                    });
                }
                else{
                    callback(null,{message:false});
                }
            }
        });
        
    }

    Culttransaction.remoteMethod('dashboard',{
        accepts: [
            {
                "arg": "cultUserId",
                "type": "string",
                "required": true,
                "description": "",
                'http':{source:'query'}
            }
        ],
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/dashboard",
            "verb": "get"
          }
        ]

    })

    Culttransaction.remoteMethod('transactionVerification', {
        accepts: [
            {
                "arg": "cultTransactionId",
                "type": "string",
                "required": true,
                "description": "",
            },
            {
                "arg": "cultTransactionOtp",
                "type": "string",
                "required": true,
                "description": "",
            }
        ],
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/verifyTransaction",
            "verb": "post"
          }
        ]
    });

};
